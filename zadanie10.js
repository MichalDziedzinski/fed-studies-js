class pojazd
{
    constructor(type,color,engine)
    {
        this.type=type;
        this.color=color;
        this.engine=engine;
        this.status=0;
    }
    showType()
    {
        return `Pojazd typu: ${this.type}`;
    }
    showColor() 
    {
        return `Pojazd koloru: ${this.color}`;
    }
    showEngine() 
    {
        if (engine=='elektryczny')
        {
            return `${this.engine} jest ekologiczny`;
        }
        else if (engine=='benzynowy'||engine=='diesel')
        {
            return `${this.engine} jest nie-ekologiczny. Elon Musk nie jest
            zadowolony z Twojego wyboru.`;
        }
    }
    counter()
    {
        let value = Math.floor(Math.random()*(90+1)+10)*100;
        if (value>2500)
        {
            this.status=value;
            return value;
        }
        else
        {
            this.status=value;
            return 'Silnik jest niedotarty'
        }
    }
    statusValue()
    {
        return `Stan licznika to: ${this.status} km.`;
    }
    interfejs()
    {
        let pojazd_div = document.createElement('div');
        let paragraph = document.createElement('p');
        let button = document.createElement('button');
        let textnode = document.createTextNode('Usuń');
        button.appendChild(textnode);

        pojazd_div.appendChild(paragraph);
        pojazd_div.appendChild(button);
        textnode = document.createTextNode(this.showType());
        garaz1.add(this.showType());
        paragraph.appendChild(textnode);
        pojazd_div.classList.add(this.type);
        var body =  document.querySelector('body');
        body.appendChild(pojazd_div);

        button.addEventListener('click', function(){
            body.removeChild(pojazd_div);
        })
    }
}

class motocykl extends pojazd
{
    constructor(color)
    {
        super('motocykl',color,'benzynowy');
        super.counter();
    } 
    jednoslad()
    {
        return `Pojazd jest jednośladem`
    }
}

class samochod extends pojazd
{
    constructor(color, engine, added)
    {
        super('samochód',color, engine);
        this.added = added;
    } 
    counter()
    {
        super.counter();
        this.status += this.added;
        return this.status;
    }
    indestructible()
    {
        if (this.color==='czerwony')
        {
            return `Samochód jest niezniszczalny, bo ma kolor ${this.color}`;
        }
    }
}

class garaż
{
    constructor()
    {
        this.tekst = 'Lista pojazdów';
        this.garaz = [];
    }
    add(pojazd){
        this.garaz.push(pojazd);
    }
    delete(){

    }
} 

const garaz1 = new garaż();

const pojazd1 = new pojazd('samochód','zielony','diesel');
pojazd1.interfejs();
console.log(pojazd1.showType(),pojazd1.showColor(),pojazd1.counter(), pojazd1.statusValue());

const motocykl1 = new motocykl('niebieski');
console.log(motocykl1.type, motocykl1.showColor(), motocykl1.engine, motocykl1.jednoslad(), motocykl1.status);

const samochod1 = new samochod('czerwony', 'diesel', 3);
console.log(samochod1.type, samochod1.color, samochod1.engine, samochod1.counter(), samochod1.indestructible());

motocykl1.interfejs();

samochod1.interfejs();





