
function createKonto(imie, nazwisko) {
    var nowe = {
        imie,
        nazwisko,
        stan: 0,
    };
    nowe.stanKonta = function(kwota){
        this.stan = kwota;
        console.log('Ilość Bitcoin na koncie:', this.stan);
        if (this.stan>0 && this.stan<=5){
            console.log('Posiadane środki możesz wypłacić bez prowizji.');
        }
        else{
            console.log('Posiadane środki możesz wypłacić z prowizją: 0,00012 BTC.');
        }
    }
    nowe.show = function(){
        document.getElementById('content').innerHTML=`Witaj ${this.imie} ${this.nazwisko} <br> ${this.stan}`;
    }
    return nowe;
}

var konto = createKonto('Jan', 'Kowalski');
konto.stanKonta(10);
konto.show();