var buttonData = document.getElementById('buttonData');
var buttonReviews = document.getElementById('buttonReviews');
var buttonAvailability = document.getElementById('buttonAvailability');
var data = document.getElementById('data');
var reviews = document.getElementById('reviews');
var availability = document.getElementById('availability');

buttonData.addEventListener('click', function () {
  showHide(data, buttonData)
});
buttonReviews.addEventListener('click', function () {
  showHide(reviews, buttonReviews)
});
buttonAvailability.addEventListener('click', function () {
  showHide(availability, buttonAvailability)
});

function showHide(element, button) {
  var isVisible = element.getAttribute("style");
  var buttonValue = button.getAttribute("value");
  var replacedValue;
  if (isVisible == 'display: none;') {
    replacedValue = buttonValue.replace("Pokaż", "Ukryj");
    button.setAttribute('value', replacedValue)
    element.setAttribute('style', 'display: block;');
    return false;
  }
  replacedValue = buttonValue.replace("Ukryj", "Pokaż");
  button.setAttribute('value', replacedValue)
  element.setAttribute('style', 'display: none;');
}