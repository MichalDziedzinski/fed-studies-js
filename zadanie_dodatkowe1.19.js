var alert = document.getElementById('alert');
var mass = document.getElementById('mass');
var btn1 = document.getElementById('btn1');
var field1 = document.getElementById('field1');
var cargo = 0;

btn1.addEventListener('click', function () {
  addCargo = parseFloat(field1.value);
  if (isNaN(addCargo) || addCargo<0){
    alert.textContent='Podaj prawidłową wartość';
  }
 cargo+=addCargo;
 if (cargo>1000){
   alert.textContent='Limit przekroczony'
   cargo-=addCargo;
   return false;
 }
 alert.textContent='';
 mass.textContent=cargo;
});