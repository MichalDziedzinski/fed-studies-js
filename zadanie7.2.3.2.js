var field1=document.getElementById('field1');
var btn_1=document.getElementById('btn1');
var para_1=document.getElementById('para_1');
var arr=[];

btn_1.addEventListener('click', function(){
    var field1_v=field1.value;
    var length=field1_v.length;
    for (var i=0; i<length;i++)
    {
        arr[i]=field1_v[i];
    }
    function notUnique(value, index, arr) { //funkcja sprawedza czy dana wartość jest pierwsza, jeśli nie, to zwraca false i nie przekazuje elementu do tablicy
        return arr.indexOf(value) !== index;
    }
    function onlyUnique(value, index, arr) {
        return arr.indexOf(value) === index;
    }
    arr = arr.filter( notUnique );
    arr = arr.filter( onlyUnique );

para_1.textContent=arr;
});