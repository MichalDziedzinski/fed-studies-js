var buttonData = document.getElementById('buttonData');
var buttonReviews = document.getElementById('buttonReviews');
var buttonAvailability = document.getElementById('buttonAvailability');
var buttons = document.querySelectorAll('.button');
var elements = document.querySelectorAll('.description');
var data = document.getElementById('data');
var reviews = document.getElementById('reviews');
var availability = document.getElementById('availability');

buttonData.addEventListener('click', function () {
  showHide(data, buttonData)
});
buttonReviews.addEventListener('click', function () {
  showHide(reviews, buttonReviews)
});
buttonAvailability.addEventListener('click', function () {
  showHide(availability, buttonAvailability)
});

function showHide(element, button) {
  var isVisible = element.getAttribute("style");
  var buttonValue = button.getAttribute("value");
  var replacedValue;
  for(var i=0; i<buttons.length; i++){
    replacedValue = buttons[i].getAttribute('value').replace("Ukryj", "Pokaż");
    buttons[i].setAttribute('value', replacedValue)
    elements[i].setAttribute('style', 'display: none;');
  }
  if (isVisible == 'display: none;') {
    replacedValue = buttonValue.replace("Pokaż", "Ukryj");
    button.setAttribute('value', replacedValue)
    element.setAttribute('style', 'display: block;');
  }
}