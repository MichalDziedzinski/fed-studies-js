class konto
{
    constructor(numer,stan)
    {
        this.numer=numer;
        this.stan=stan;
    }
    income(cash)
    {
        if (cash<0){
            return 'Kwota wpłaty nie może być mniejsza od 0'
        }
        this.stan+=cash;
        return this.stan;
    }
    expense(cash)
    {
        if(cash>this.stan){
            return 'Kwota wypłaty jest większa od kwoty na koncie'
        } else if (cash<0){
                return 'Kwota wpłaty nie może być mniejsza od 0'
        }
        this.stan-=cash;
        return this.stan;
    }
    interface(){
        let konto_div = document.createElement('div');
        let interfejs = `
        <p>Konto numer: ${this.numer}</p>
        <p>Rodzaj konta:<p>
        <input type="radio" name="account_type" value="male" checked> Normalne
        <input type="radio" name="account_type" value="female"> Vip
        <input type="radio" name="account_type" value="other"> Walutowe
        <p id="paragraph_${this.numer}">Stan konta: ${this.stan}</p>
        <input id="income_input_${this.numer}" type="text">
        <input id="income_${this.numer}" type="button" value="Wpłać">
        <input id="expense_input_${this.numer}"  type="text">
        <input id="expense_${this.numer}" type="button" value="Wypłać">
        <input id="delete_${this.numer}" type="button" value="Wykasuj">
        `;
        konto_div.innerHTML = interfejs;
        konto_div.classList.add('account');
        let body =  document.querySelector('body');
        body.appendChild(konto_div);

        var button_income = document.getElementById(`income_${this.numer}`);

        var button_expense = document.getElementById(`expense_${this.numer}`);
        var button_delete = document.getElementById(`delete_${this.numer}`);

        var input_income = document.getElementById(`income_input_${this.numer}`);

        var input_expense = document.getElementById(`expense_input_${this.numer}`);

        var paragraph = document.getElementById(`paragraph_${this.numer}`);


        button_income.addEventListener('click',() =>{
            this.income(parseFloat(input_income.value));
            paragraph.textContent = `Stan konta: ${this.stan}`;
        });

        button_expense.addEventListener('click',() =>{
            this.expense(parseFloat(input_expense.value));
            paragraph.textContent = `Stan konta: ${this.stan}`;
        });
    }
}

const konto1 = new konto(123456789, 100);
const konto2 = new konto(111111111, 1000);
const konto3 = new konto(222222222, 10540);

konto1.interface();
konto2.interface();
konto3.interface();


