
class person {
    constructor(imie, nazwisko, wiek){
        this.imie=imie;
        this.nazwisko=nazwisko;
        this.wiek=wiek;
    }
}
class bitCoin {
    constructor(ile){
        this.ile=ile;
    }
}

var obiektA = new person('Jan', 'Kowalski', 20);

console.log(obiektA);

var obiektB = new bitCoin(50);

console.log(obiektB);

var obiektC = {...obiektA};//kopiowanie obiektu person1 

obiektC.imie = 'Szymon';
obiektC.nazwisko='Nowak';
obiektC.wiek=30;

console.log(obiektC);

var obiektD = {...obiektB, ...obiektC};

console.log(obiektD);