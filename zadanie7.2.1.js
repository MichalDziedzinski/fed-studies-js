var field1=document.getElementById('field1');
var field2=document.getElementById('field2');
var btn_1=document.getElementById('btn1');
var para=document.getElementById('para');

btn_1.addEventListener('click', function(){
    var field1_v=field1.value;
    var field2_v=field2.value;
    var counter=0;
    if (field2_v==1)
    {
      for (var i=0; i<field1_v.length;i++)
      {
          if (field1_v[i]==field2_v)
          {
              counter++;
          }
      }
      para.textContent=counter;
    }
    else
    {
      //musiałem stworzyć nowe wyrażenie regularne, ponieważ aby funkcja działała jak w przykładzie:
      // var str = "The rain in SPAIN stays mainly in the plain";
      // var res = str.match(/ain/g);
      // The result of res will be an array with the values:
      //
      // ain,ain,ain

      // pierwszy argument (w tym przypadku ain) nie może być zmienną tylko musi być stringiem.
      var re = new RegExp(field2_v, 'g');
      var res=field1_v.match(re);
      para.textContent=res.length;
    }

});
