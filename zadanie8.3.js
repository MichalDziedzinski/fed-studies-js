var body = document.getElementsByTagName('body')[0];
var table = document.createElement('table');
var tbody = document.createElement('tbody');

table.border = "1";

body.appendChild(table);
table.appendChild(tbody);
var indexRow = 1;

for (var r = 0; r < 3; r++) {
  var myRow = document.createElement('tr');
  //var indexRow = 0;
  
  for(var c = 0; c < 3; c++) {
    var myCol = document.createElement('td');
    var indexCol = c +1;
    var textCell = 'przykładowy tekst';
    var textNode = document.createTextNode(textCell);

    myRow.appendChild(myCol);
    myCol.appendChild(textNode)
    indexRow++;
  }
  table.appendChild(myRow);
}
var cells=document.querySelectorAll('td');
var rows=document.querySelectorAll('tr');
var cellsLen=cells.length;
var rowsLen=rows.length;
var cellsPerRow=cellsLen/rowsLen;

var cellNumber = 4;

cells[cellNumber].bgColor='black';
document.addEventListener('keydown', function(ev){
  console.log(ev.keyCode);
  switch(ev.keyCode){
    case 37:
    if (cellNumber%cellsPerRow!=0){
      cells[cellNumber].bgColor='white';
      cellNumber-=1;
      cells[cellNumber].bgColor='black';
    }
    break;
    case 38:
    if (cellNumber>cellsPerRow-1){
      cells[cellNumber].bgColor='white';
      cellNumber-=cellsPerRow;
      cells[cellNumber].bgColor='black';
    }
    break;
    case 39:
    if (cellNumber%(cellsPerRow)!=(cellsPerRow-1)){
      cells[cellNumber].bgColor='white';
      cellNumber+=1;
      cells[cellNumber].bgColor='black';
    }
    break;
    case 40:
    if (cellNumber<(cellsLen-cellsPerRow)){
      cells[cellNumber].bgColor='white';
      cellNumber+=cellsPerRow;
      cells[cellNumber].bgColor='black';
    }
    break;
  }
});