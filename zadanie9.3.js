
function createKonto(imie, nazwisko) {
    this.imie=imie;
    this.nazwisko=nazwisko;
    this.stan=0
    this.stanKonta = function(kwota){
        // this.stan = kwota;
        console.log('Ilość Bitcoin na koncie:', this.stan);
        if (this.stan>0 && this.stan<=5){
            console.log('Posiadane środki możesz wypłacić bez prowizji.');
        }
        else{
            console.log('Posiadane środki możesz wypłacić z prowizją: 0,00012 BTC.');
        }
    }
    this.show = function(){
        document.getElementById('content').innerHTML=`Witaj ${this.imie} ${this.nazwisko} <br> Stan konta: ${this.stan}`;
    }
    this.wplac = function(kwota){
        this.stan+=kwota;
        console.log(this.stan);
    }
    this.wyplac = function(kwota){
        this.stan-=kwota;
        console.log(this.stan);
    }
}

var konto1 = new createKonto('Jan', 'Kowalski');
konto1.stanKonta(10);
konto1.show();
konto1.wplac(100);
konto1.show();
konto1.wyplac(97);
konto1.show();