var div=document.getElementsByClassName('div_div');
//przyciski
var textB=document.getElementById('textB');
var colorB=document.getElementById('colorB');
var sizeB=document.getElementById('sizeB');
var boldB=document.getElementById('boldB');
var italicB=document.getElementById('italicB');
var backgroundB=document.getElementById('backgroundB');
var marginB=document.getElementById('marginB');
var paddingB=document.getElementById('paddingB');
var border_sizeB=document.getElementById('border_sizeB');
var border_colorB=document.getElementById('border_colorB');
var shadowB=document.getElementById('shadowB');
//inputy
var text=document.getElementById('text');
var color=document.getElementById('color');
var size=document.getElementById('size');
var bold=document.getElementsByName('bold');
var italic=document.getElementsByName('italic');
var background=document.getElementById('background');
var margin=document.getElementById('margin');
var padding=document.getElementById('padding');
var border_size=document.getElementById('border_size');
var border_color=document.getElementById('border_color');
var shadow=document.getElementsByName('shadow');

textB.addEventListener('click', function(){
    textV=text.value;
    div[0].textContent=textV;
});

colorB.addEventListener('click', function(){
    colorV=color.value;
    div[0].style.color=colorV;
});

sizeB.addEventListener('click', function(){
    sizeV=size.value;
    div[0].style.fontSize=sizeV+'px';
});

boldB.addEventListener('click', function(){
    for (var i = 0, length = bold.length; i < length; i++)
    {
     if (bold[i].checked)
     {
      // do whatever you want with the checked radio
      boldV=bold[i].value;
      if (boldV==1) 
        div[0].style.fontWeight='bold';
      // only one radio can be logically checked, don't check the rest
      break;
     }
    }
});

italicB.addEventListener('click', function(){
    for (var i = 0, length = italic.length; i < length; i++)
    {
     if (italic[i].checked)
     {
      // do whatever you want with the checked radio
      italicV=italic[i].value;
      if (italicV==1) 
        div[0].style.fontStyle='italic';
      // only one radio can be logically checked, don't check the rest
      break;
     }
    }
});

backgroundB.addEventListener('click', function(){
    backgroundV=background.value;
    div[0].style.backgroundColor=backgroundV;
});

marginB.addEventListener('click', function(){
    marginV=margin.value;
    div[0].style.margin=marginV+'px';
});

paddingB.addEventListener('click', function(){
    paddingV=padding.value;
    div[0].style.padding=paddingV+'px';
});

border_sizeB.addEventListener('click', function(){
    border_sizeV=border_size.value;
    div[0].style.borderStyle='solid';
    div[0].style.borderWidth=border_sizeV+'px';
});

border_colorB.addEventListener('click', function(){
    border_colorV=border_color.value;
    div[0].style.borderColor=border_colorV;
});
shadowB.addEventListener('click', function(){
    for (var i = 0, length = shadow.length; i < length; i++)
    {
     if (shadow[i].checked)
     {
      // do whatever you want with the checked radio
      if (italicV==1) 
        div[0].style.textShadow='2px 2px #ff0000';
      // only one radio can be logically checked, don't check the rest
      break;
     }
    }
});