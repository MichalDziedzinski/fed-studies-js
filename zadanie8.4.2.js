var tab = ['pierwszy', 'drugi','trzeci', 'czwarty','piąty','szósty','siódmy','ósmy','dziewiąty','dziesiąty'];

var input = document.getElementById('input');
var prevButton = document.getElementById('previous');
var nextButton = document.getElementById('next');

var number = 0;

input.value=tab[number];

prevButton.addEventListener('click', previous);
nextButton.addEventListener('click', next);
document.addEventListener('keydown', function(ev){
  switch(ev.keyCode){
    case 37:
      previous();
    break;
    case 39:
      next();
    break;
  }
});

function previous(){
  if(number==0){
    prevButton.setAttribute('disabled');
  }
  tab[number]=input.value;
  input.value=tab[--number];
}

function next(){
  if(number==tab.length-1){
    nextButton.setAttribute('disabled');
  }
  tab[number]=input.value;
  input.value=tab[++number];
}