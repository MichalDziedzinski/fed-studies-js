var show = document.getElementById('show');
var content = document.getElementById('content');

var isVisible = false;

show.addEventListener('click', showHide);

function showHide(){
  if(!isVisible){
    content.setAttribute('style','display: block;');
    show.setAttribute('value','Ukryj;');
    isVisible=true;
    return false;
  }
  show.setAttribute('value','Pokaż;');
  content.setAttribute('style','display: none;');
  isVisible=false;
}
