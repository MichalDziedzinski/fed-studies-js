var body = document.getElementsByTagName('body')[0];
var field1=document.getElementById('field1');
var field2=document.getElementById('field2');
var btn_1=document.getElementById('btn1');
var btn_2=document.getElementById('btn2');
var btn_3=document.getElementById('btn3');
var btn_4=document.getElementById('btn4');
var btn_5=document.getElementById('btn5');
var btn_6=document.getElementById('btn6');
var btn_7=document.getElementById('btn7');
var tab=[];
var arr=[];
var arry=[];

btn_1.addEventListener('click', f_1);
btn_2.addEventListener('click', f_2);
btn_3.addEventListener('click', f_3);
btn_4.addEventListener('click', f_4);
btn_5.addEventListener('click', f_5);
btn_6.addEventListener('click', f_6);
btn_7.addEventListener('click', f_7);

function f_1(){
  {

    var table = document.createElement('table');
    var tbody = document.createElement('tbody');
    var ile=parseInt(field1.value,10);
    var zakres=parseInt(field2.value,10);

    for (var i=0; i<ile; i++)
    {
      tab[i]=Math.floor(Math.random()*zakres+1);
    }

    table.border = "1";

    body.appendChild(table);
    table.appendChild(tbody);
    var indexRow = 1;//numer wiersza

    for (var r = 0; r < tab.length; r++)
    {
      var myRow = document.createElement('tr');//tworzenie wierszy
      var myCol = document.createElement('td');//tworzenie kolumn
      var textNode = document.createTextNode(tab[r]);
      myRow.appendChild(myCol);
      myCol.appendChild(textNode);
      table.appendChild(myRow);
    }
  }
}

function f_2(){
  tab.sort(function(a,b){
    return a-b;
  });
  document.getElementsByTagName('table')[0].remove();

  var table = document.createElement('table');
  var tbody = document.createElement('tbody');

  table.border = "1";

  body.appendChild(table);
  table.appendChild(tbody);
  var indexRow = 1;//numer wiersza

  for (var r = 0; r < tab.length; r++)
  {
    var myRow = document.createElement('tr');//tworzenie wierszy
    var myCol = document.createElement('td');//tworzenie kolumn
    var textNode = document.createTextNode(tab[r]);
    myRow.appendChild(myCol);
    myCol.appendChild(textNode);
    table.appendChild(myRow);
  }
}

function f_3(){
  tab.sort(function(a,b){
    return b-a;
  });
  document.getElementsByTagName('table')[0].remove();

  var table = document.createElement('table');
  var tbody = document.createElement('tbody');

  table.border = "1";

  body.appendChild(table);
  table.appendChild(tbody);
  var indexRow = 1;//numer wiersza

  for (var r = 0; r < tab.length; r++)
  {
    var myRow = document.createElement('tr');//tworzenie wierszy
    var myCol = document.createElement('td');//tworzenie kolumn
    var textNode = document.createTextNode(tab[r]);
    myRow.appendChild(myCol);
    myCol.appendChild(textNode);
    table.appendChild(myRow);
  }
}

function f_4(){
  var j=0;
  for(var i=0;i<tab.length;i++)
  {
    if(tab[i]%2==0)
    {
      arr[j]=tab[i];
      j++;
    }
    document.getElementsByTagName('table')[0].remove();

    var table = document.createElement('table');
    var tbody = document.createElement('tbody');

    table.border = "1";

    body.appendChild(table);
    table.appendChild(tbody);
    var indexRow = 1;//numer wiersza

    for (var r = 0; r < arr.length; r++)
    {
      var myRow = document.createElement('tr');//tworzenie wierszy
      var myCol = document.createElement('td');//tworzenie kolumn
      var textNode = document.createTextNode(arr[r]);
      myRow.appendChild(myCol);
      myCol.appendChild(textNode);
      table.appendChild(myRow);
    }
  }
}

function f_5(){
  var j=0;
  for(var i=0;i<tab.length;i++)
  {
    if(tab[i]%2!=0)
    {
      arr[j]=tab[i];
      j++;
    }
    document.getElementsByTagName('table')[0].remove();

    var table = document.createElement('table');
    var tbody = document.createElement('tbody');

    table.border = "1";

    body.appendChild(table);
    table.appendChild(tbody);
    var indexRow = 1;//numer wiersza

    for (var r = 0; r < arr.length; r++)
    {
      var myRow = document.createElement('tr');//tworzenie wierszy
      var myCol = document.createElement('td');//tworzenie kolumn
      var textNode = document.createTextNode(arr[r]);
      myRow.appendChild(myCol);
      myCol.appendChild(textNode);
      table.appendChild(myRow);
    }
  }
}

function f_6(){
  {
    document.getElementsByTagName('table')[0].remove();
    var table = document.createElement('table');
    var tbody = document.createElement('tbody');

    table.border = "1";

    body.appendChild(table);
    table.appendChild(tbody);
    var indexRow = 1;//numer wiersza

    for (var r = 0; r < tab.length; r++)
    {
      var myRow = document.createElement('tr');//tworzenie wierszy
      var myCol = document.createElement('td');//tworzenie kolumn
      var textNode = document.createTextNode(tab[r]);
      myRow.appendChild(myCol);
      myCol.appendChild(textNode);
      table.appendChild(myRow);
    }
  }
}
function f_7(){
  var ile_rows=document.getElementsByTagName('tr');
  for (var i=0; i<ile_rows.length; i++)
  {
    arry[i]=parseInt(ile_rows[i].textContent);
  }
  document.getElementsByTagName('table')[0].remove();
  var table = document.createElement('table');
  var tbody = document.createElement('tbody');

  table.border = "1";

  body.appendChild(table);
  table.appendChild(tbody);
  var indexRow = 1;//numer wiersza

  for (var r = 0; r < Math.ceil(arry.length/2); r++)
  {
    var myRow = document.createElement('tr');//tworzenie wierszy
    for(var c = 0; c<2; c++) {
      var myCol = document.createElement('td');//tworzenie kolumn
      switch (c) {
        case 0:
          var textNode = document.createTextNode(arry[r]);
          break;
        case 1:
          var textNode = document.createTextNode(arry[r+Math.ceil(arry.length/2)]);
          break;
        default:
      }
      myRow.appendChild(myCol);
      myCol.appendChild(textNode);
      indexRow+=Math.ceil(arry.length/2);
    }
    indexRow=r+2;
    table.appendChild(myRow);
  }
}
